import Vue from "vue";
import VueRouter from "vue-router";
import PageView from "../views/PageView.vue";

Vue.use(VueRouter);

const routes = [
  {
    path: "/",
    name: "PageView",
    component: PageView,
  },
];

const router = new VueRouter({
  mode: "history",
  base: process.env.BASE_URL,
  routes,
});

export default router;

