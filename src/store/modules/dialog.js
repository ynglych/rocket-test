const state = {
  chatUsers: [
    {
      name: "Администратор",
      imgSrc: require("@/assets/img/user-ava--2.jpg"),
      flag: "TravelAsk",
      rating: null,
      status: "admin",
    },
    {
      name: "Наталия Полянская",
      imgSrc: require("@/assets/img/user-ava.jpg"),
      flag: "Гид по Баварии, фотограф",
      rating: 2,
      status: "user",
    },
  ],
  messages: [],
};

const mutations = {
  addMessage(state, message) {
    state.messages.push(message);
    localStorage.setItem("vuexMessages", JSON.stringify(state.messages));
  },
};

const actions = {
  sendMessage({ commit }, message) {
    commit("addMessage", message);
  },
};

const getters = {
  getChatUsers(state) {
    return state.chatUsers;
  },
  getMessages(state) {
    return state.messages;
  },
};

export default {
  namespaced: true,
  state,
  mutations,
  actions,
  getters,
};
